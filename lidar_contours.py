from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
from rplidar import RPLidar
import multiprocessing

def preview():
	camera = PiCamera()
	camera.resolution = (640,480)
	camera.contrast = -10
	camera.framerate = 60
	camera.brightness = 50
	rawCapture = PiRGBArray(camera, size=(640,480))
	time.sleep(0.1)

	for frame in camera.capture_continuous(rawCapture, format = "bgr", use_video_port = True):

		image = cv2.cvtColor(frame.array,cv2.COLOR_BGR2GRAY)

		ret, thresh = cv2.threshold(image, 100, 255, 0)
		im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

		im2 = cv2.drawContours(im2, contours, -1, (255,0,0), 1)
		im3 = cv2.cvtColor(im2, cv2.COLOR_GRAY2BGR)
		images = np.hstack((frame.array,im3))

		cv2.imshow("Frame", images)
		key = cv2.waitKey(1) & 0xFF

		rawCapture.truncate(0)

		if key == ord("q"):
			break

def capture(outfile1,outfile2):
	camera = PiCamera()
	camera.resolution = (320,240)
	camera.contrast = -10
	camera.framerate = 60
	camera.brightness = 50
	rawCapture = PiRGBArray(camera, size=(320,240))
	time.sleep(0.1)

	all_frames = []
	image_times = []
	i = 0
	for frame in camera.capture_continuous(rawCapture, format = "bgr", use_video_port = True):

		image = cv2.cvtColor(frame.array,cv2.COLOR_BGR2GRAY)

		ret, thresh = cv2.threshold(image, 100, 255, 0)
		im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

		im2 = cv2.drawContours(im2, contours, -1, (255,0,0), 1)
		im3 = cv2.cvtColor(im2, cv2.COLOR_GRAY2BGR)
		images = np.hstack((frame.array,im3))
		all_frames.append(images)
		image_times.append(time.time())
		#cv2.imshow("Frame", images)
		#key = cv2.waitKey(1) & 0xFF

		rawCapture.truncate(0)
		i += 1
		if i >= 60:
			break
		'''
		if key == ord("q"):
			break
		'''
	np.save(outfile1, all_frames)
	np.save(outfile2, image_times)
'''
def print_scans(N_SCANS):
	lidar = RPLidar("/dev/ttyUSB1")
	lidar.clear_input()
	scans = []
	for i, scan in enumerate(lidar.iter_scans()):
        	print(i)
        	time.sleep(0.1)
		if i >= N_SCANS:
	    		print "done"
    			lidar.stop()
			lidar.stop_motor()
			lidar.clear_input()
			lidar.disconnect()
    			break
'''
def test_scans():
	lidar = RPLidar("/dev/ttyUSB0")
	time.sleep(0.1)
	#lidar.clear_input()
	print(lidar.get_health())
	for i, scan in enumerate(lidar.iter_scans()):
		print(i)
		if i >= 30:
			print("done")
			lidar.disconnect()
			time.sleep(0.1)
			break

if __name__ == "__main__":
	p1 = multiprocessing.Process(target= capture, args = ["ims1.npy","imts1.npy"])
	p2 = multiprocessing.Process(target = test_scans)
	p2.start()
	p1.start()
	#preview()
	#print_scans(30)
	#raw_input("Done!")
