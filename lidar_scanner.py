import rplidar
import numpy as np
import time
import sys

PORT = "/dev/ttyUSB0"
'''
lidar = rplidar.RPLidar(PORT)
info =lidar.get_info()
print('\n'.join('%s: %s' % (k, str(v)) for k, v in info.items()))
N_SCANS = 100
my_scanlist = []
for i, scan in enumerate(lidar.iter_scans()):
    my_scanlist.append(scan)
    print i
    if i >= N_SCANS:
        lidar.stop_motor()
        lidar.disconnect()
        break
print my_scanlist
my_scanarray = np.array(my_scanlist)
print my_scanarray
np.save("myscans.npy", my_scanarray)
'''
def print_scans(N_SCANS):
    lidar = rplidar.RPLidar(PORT)
    scans = []
    for i, scan in enumerate(lidar.iter_scans()):
        print(scan)
        if i >= N_SCANS:
            print "done"
            lidar.stop_motor()
            lidar.stop()
            lidar.clear_input()
            lidar.disconnect()
            break

def grab_frame(outfile):
    lidar = rplidar.RPLidar(PORT)
    scans = []
    for i, scan in enumerate(lidar.iter_scans()):
        if i == 30:
            scans.append(scan)
            np.save(outfile,scans)
            lidar.stop()
            lidar.stop_motor()
            lidar.clear_input()
            lidar.disconnect()
            break

def save_scans(outfile, N_SCANS):
    lidar = rplidar.RPLidar(PORT)
    scans = []
    for i, scan in enumerate(lidar.iter_scans()):
        print(i)
        scans.append(scan)
        if i >= N_SCANS:
            print "done"
            np.save(outfile, scans)
            lidar.stop_motor()
            lidar.disconnect()
            break

if __name__=='__main__':
    print_scans(100)
    #grab_frame('oneframe.npy')
    #save_scans("outfile1.npy", 100)
