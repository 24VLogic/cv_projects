import multiprocessing
from rplidar import RPLidar

def test():
    lidar = RPLidar("/dev/ttyUSB0")
    for i, scan in enumerate(lidar.iter_scans()):
        print(i)
        if i >= 10:
            lidar.disconnect()
            break
p1 = multiprocessing.Process(target=test)
p1.start()
